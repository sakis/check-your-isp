# Check your ISP

## What's this?

It's a script that every 10 minutes runs a speedtest and allows you to view the results in a simple graph.
It's Good Enough for getting a rough idea about how your ISP is doing in providing you the level of service you're paying for.

## What it's NOT:

A comprehensive production-ready well-written tool.

## How do I use it?

Create a virtualenv and activate it by running

`virtualenv venv && source venv/bin/activate`

Install dependencies

`make deps`

Execute the script

`python checkyourisp.py`

### Results

To view your results in a simple graph just doubleclick `results.html`

The results are stored in `results.json`.

### Example output

![Imgur](http://i.imgur.com/6ejUnAV.png)
