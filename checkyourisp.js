var CheckYourISP = (function() {
    var checkYourISP = {
        metrics: ['download', 'upload'],
        data: {},

        fix_data: function() {
            CheckYourISP.metrics.forEach(function(metric, metricIndex) {
                if (! (metric in CheckYourISP.data) ) {
                    CheckYourISP.data[metric] = [];
                }

                window.DATA.forEach(function(item, index) {
                    CheckYourISP.data[metric].push(
                        {
                            'x': item.timestamp,
                            'y': parseFloat(item[metric] / 1000000).toFixed(2)
                        }
                    );
                });
            });
        },

        create_graphs: function() {
            CheckYourISP.metrics.forEach(function(metric, metricIndex) {
                var ctx = $("#speed-" + metric);
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        datasets: [{
                            label: metric + " speed",
                            fill: false,
                            borderColor: "#42b6f4",
                            pointBackgroundColor: "#000000",
                            pointRadius: 3,
                            borderWidth: 2,
                            lineTension: 0,
                            data: CheckYourISP.data[metric]
                        }]
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                type: 'time',
                                position: 'bottom'
                            }]
                        }
                    }
                });
            });
        },

        populate_stats: function() {
            CheckYourISP.metrics.forEach(function(metric, metricIndex) {
                var speeds = CheckYourISP.data[metric].map(function(item){return item.y;});
                var sum = 0;
                var avg = 0;

                if (speeds.length > 0) {
                    for(var i = 0; i < speeds.length; i++ ) {
                        sum += parseFloat(speeds[i]);
                    }
                    avg = sum / speeds.length
                }

                $("#max-speed-" + metric).html(Math.max.apply(null, speeds));
                $("#min-speed-" + metric).html(Math.min.apply(null, speeds));
                $("#avg-speed-" + metric).html(avg.toFixed(2));
            });

        },

        init: function() {
            this.fix_data();
            this.create_graphs();
            this.populate_stats();
        }
    };

    return checkYourISP;
})();

Chart.defaults.global.maintainAspectRatio = false;
CheckYourISP.init();
