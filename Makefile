.PHONY: venv
venv:
	@ . venv/bin/activate

.PHONY: deps
deps: venv
	pip install -r requirements.txt
