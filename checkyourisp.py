import datetime
import json
import os
from subprocess import Popen, PIPE

from retrying import retry

RESULTS_JSON_FILE = 'results.json'
RESULTS_JS_VAR = 'results.js'


def load_existing_results():
    if not os.path.isfile(RESULTS_JSON_FILE):
        return []

    with open(RESULTS_JSON_FILE) as fp:
        return json.load(fp)


def update_json(result):
    results = load_existing_results()
    results.append(result)

    with open(RESULTS_JSON_FILE, 'w') as fp:
        json.dump(results, fp)

    return results


def update_js_file(results):
    output = "var DATA = {};".format(json.dumps(results))
    with open(RESULTS_JS_VAR, 'w') as fp:
        fp.write(output)


@retry(wait_fixed=(10 * 60 * 1000), retry_on_result=lambda result: True)
def call_speedtest_cli():
    print("Running speedtest at: {}".format(datetime.datetime.now()))
    command = "speedtest --json --secure".split()
    process = Popen(command, stdout=PIPE, stderr=PIPE)
    out, err = process.communicate()

    if err:
        return

    result = json.loads(out)
    all_results = update_json(result)
    update_js_file(all_results)


if __name__ == '__main__':
    call_speedtest_cli()
